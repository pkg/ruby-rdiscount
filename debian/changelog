ruby-rdiscount (2.1.8-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 23:59:10 +0000

ruby-rdiscount (2.1.8-2) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 01 Jul 2022 00:25:10 +0100

ruby-rdiscount (2.1.8-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 22 Feb 2021 02:11:35 +0000

ruby-rdiscount (2.1.8-1) unstable; urgency=medium

  * Team upload.
  * Bumped up compat version to 9.
  * Upload to unstable.
  * Multi-arch used by default now (Closes: #723535)

 -- Sebastien Badia <seb@sebian.fr>  Wed, 19 Aug 2015 17:58:04 +0200

ruby-rdiscount (2.1.8-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release (2.1.8).
  * Target experimental and build with ruby 2.2.
  * Add ruby-test-unit to Build-Depends for ruby2.2.
  * Update Vcs-Browser to cgit URL and HTTPS.
  * Bump Standards-Version to 3.9.6 (no further changes).

 -- Sebastien Badia <seb@sebian.fr>  Sun, 19 Apr 2015 20:13:01 +0200

ruby-rdiscount (2.1.7.1-2) unstable; urgency=medium

  * Team upload
  * Actually use system libmarkdown

 -- Alessandro Ghedini <ghedo@debian.org>  Tue, 08 Jul 2014 19:34:50 +0200

ruby-rdiscount (2.1.7.1-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Alessandro Ghedini <ghedo@debian.org>  Mon, 14 Apr 2014 12:49:44 +0200

ruby-rdiscount (2.1.7-1.1) unstable; urgency=medium

  * NMU
  * New upstream release (Closes: #728744)
  * Bump Standards-Version to 3.9.5 (no changes needed)
  * Update upstream license
  * Use system libmarkdown (Closes: #649489)

 -- Alessandro Ghedini <ghedo@debian.org>  Sat, 12 Apr 2014 18:40:25 +0200

ruby-rdiscount (1.6.8-3) unstable; urgency=low

  * Team upload.
  * Bump build dependency on gem2deb to >= 0.3.0~.
  * Bump Standard-Version: to 3.9.3 (no changes needed)
  * Update debian/copyright to DEP-5 copyright-format/1.0

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Sat, 30 Jun 2012 00:02:04 +0200

ruby-rdiscount (1.6.8-2) unstable; urgency=low

  * Do not install markdown.7 (Closes: #646932)

 -- Martin Ueding <dev@martin-ueding.de>  Wed, 09 Nov 2011 08:54:14 -0200

ruby-rdiscount (1.6.8-1) unstable; urgency=low

  * Initial release (Closes: #643673)

 -- Martin Ueding <dev@martin-ueding.de>  Wed, 28 Sep 2011 16:19:55 +0200
